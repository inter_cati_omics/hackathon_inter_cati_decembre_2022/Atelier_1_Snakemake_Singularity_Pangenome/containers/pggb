# pggb Singularity container
### Package pggb Version 0.5.4
pggb:
This pangenome graph construction pipeline renders a collection of sequences into a pangenome graph (in the variation graph model).
Depends:
bcbcftoolsgfaffix >=0.1.4 gsl 2.7.0.* idna <3,>=2.5 multiqc >=1.11 odgi >=0.8.1 pigzseqwish >=0.7.7 smoothxg >=0.6.7 tabixtimevg 1.40.0.* wfmash >=0.10.0

Homepage:

https://github.com/pangenome/pggb

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
pggb Version: 0.5.4<br>
Singularity container based on the recipe: Singularity.pggb_v0.5.4.def

Local build:
```
sudo singularity build pggb_v0.5.4.sif Singularity.pggb_v0.5.4.def
```

Get image help:
```
singularity run-help pggb_v0.5.4.sif
```

Default runscript: pggb<br>
Usage:
```
./pggb_v0.5.4.sif --help
```
or:
```
singularity exec pggb_v0.5.4.sif pggb --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull pggb_v0.5.4.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/pggb/pggb:latest

```

