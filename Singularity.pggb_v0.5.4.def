# distribution based on: debian 10.13
Bootstrap:docker
From:debian:10.13-slim

# container for pggb v0.5.4
# Build:
# sudo singularity build pggb_v0.5.4.sif Singularity.pggb_v0.5.4.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAe GAFL 2022
#AUTHOR Jacques Lagnel
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF MYDATEMODIF
#pggb (v0.5.4)

%help
Container for pggb version 0.5.4
pggb:
This pangenome graph construction pipeline renders a collection of sequences into a pangenome graph (in the variation graph model).
Depends:
bcbcftoolsgfaffix >=0.1.4 gsl 2.7.0.* idna <3,>=2.5 multiqc >=1.11 odgi >=0.8.1 pigzseqwish >=0.7.7 smoothxg >=0.6.7 tabixtimevg 1.40.0.* wfmash >=0.10.0
Homepage:
https://github.com/pangenome/pggb

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: pggb

Usage:
    ./pggb_v0.5.4.sif --help
    or:
    singularity exec pggb_v0.5.4.sif pggb --help

%runscript
    #default runscript: pggb passing all arguments from cli: $@
    exec /opt/miniconda/bin/pggb "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    #apt upgrade -y
    apt install -y wget bzip2

    #install conda
    cd /opt
    rm -fr miniconda

    #miniconda3: get miniconda3 version 4.11.0
    #wget https://repo.continuum.io/miniconda/Miniconda2-4.7.12-Linux-x86_64.sh -O miniconda.sh
    wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install pggb

    conda install -y -c bioconda pggb=0.5.4

    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

